<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>alert_on_close_won_opp</fullName>
        <description>alert on close won opp</description>
        <protected>false</protected>
        <recipients>
            <recipient>sagar.kakkar@grazitti.com.dev</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/templates_for_close_case</template>
    </alerts>
</Workflow>
